<?php
namespace Ponbike\RequireJsOptimizer\Block;

use \Magento\Framework\App\State as AppState;
use Magento\Framework\View\Element\Template;
use Ponbike\RequireJSOptimizer\Service\Config;

/**
 * Class Loader to include require-js-loader.js script only when in production mode
 * @package Ponbike\RequireJsOptimizer\Block
 * @author Vladimir Kerkhoff <vladimir.kerkhoff@pon.bike>
 */
class Loader extends Template
{

    /**
     * Loader constructor.
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Ponbike\RequireJSOptimizer\Service\Config       $config
     * @param array                                            $data
     */
    public function __construct(
        Template\Context $context,
        Config $config,
        array $data = []
    ) {
        parent::__construct($context, $data);

        if ($this->_appState->getMode() == AppState::MODE_PRODUCTION && $config->isOptimizerEnabled()) {
            $this->pageConfig->addPageAsset('Ponbike_RequireJSOptimizer::js/requirejs-loader.js');
        }
    }
}
