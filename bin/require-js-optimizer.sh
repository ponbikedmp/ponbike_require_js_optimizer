#!/usr/bin/env bash
if [ ! -e "require-js-optimizer-build.json" ]; then
    echo "require-js-optimizer-build.json does not exist exiting optimizer"
    exit
fi

#Fix a wrong package name for mage/requirejs/text
sed -i -e 's/mage\/requirejs\/text/requirejs\/text/g' require-js-optimizer-build.json

path='pub/static/frontend'
# List all frontend theme locales, excluding the Magento themes
dirs=`ls -d $path/*/*/* | grep -v Magento`

for dir in $dirs
do
    locale=$dir
    source=$dir"_src"

    if [ -d $locale ];
    then
        # Rename original theme to _src
        echo "Moving $locale to $source"
        mv  $locale $source

        # Run requireJS optimizer script
        echo "Running RequireJS optimizer and building theme structure in $locale"
        r_js -o require-js-optimizer-build.json \
            baseUrl=$source \
            dir=$locale \
            >> var/log/require-js-optimizer.log

        if [ $? -eq 0 ];
        then
            # Command succesfull, remove _src folder
            echo "Optimizer completed, removing $source directory!"
            rm -rf $source
        else
            # Command failed, restore original theme files
            echo "Optimizer failed, restoring $locale directory!"
            rm -rf $locale
            mv $source $locale
        fi
    fi
done

if [ -f /usr/bin/timeout ];
then
    # Run GZIP command for Magento Commerce Cloud deploys (make sure this is disabled in the .magento.env.yaml file during static content deploy
    echo "Compressing assets"
    /usr/bin/timeout -k 30 600 /bin/bash -c 'find '\''/app/pub/static/'\'' -type d -name '\''DELETING_*'\'' -prune -o -type f -size +300c '\''('\'' -name '\''*.js'\'' -or -name '\''*.css'\'' -or -name '\''*.svg'\'' -or -name '\''*.html'\'' -or -name '\''*.htm'\'' '\'')'\'' -print0 | xargs -0 -n100 -P16 gzip -q --keep -6'
fi
