#Ponbike_RequireJS Optimizer extension for Magento 2

This module will implement the config-gen module created by WeareJH (see https://github.com/WeareJH/config-gen for details) in your deploy workflow and will only add the bundles when running in production mode.

Step 1 - Download the binary
--
Download the latest `config-gen` [release](https://github.com/shakyShane/config-gen/releases) from the WeareJH config-gen repository and store it in your project bin directory. Also 

Step 2 - Edit the configuration file
---
Rename the `require-js-optimizer-config.yaml.dist` to `require-js-optimizer-config.yaml` and `require-js-optimizer-bundle.yaml.dist` to `require-js-optimizer-config.yaml`. Next edit the `require-js-optimizer-bundles.yaml` to fit your needs.

Step 3 - Collect the data
---
Next run the config-gen command:

```bin/config-gen http://example.com/ --config require-js-optimizer-config.yaml --port 12345```, next access the host [http://localhost:12345](http://localhost:12345) and visit all the pages listed in your configuration file to record the data.

Step 4 - Store the collected data in your project
---
When all pages are recorded download the recorded data:

`build.json`

The build.json data is used to run the requirejs optimizer command, store the contents of [http://localhost:12345/__bs/build.json]() to `require-js-optimizer-build.json` in your project root.

`loaders.js`

The loader.js data is used to load the bundle configuration for your theme, this file needs to be placed in the active theme and will only be included when the environment is running in production mode. Store the contents of [http://localhost:12345/__bs/loaders.js]() to your theme in the file `Ponbike_RequireJSOptimizer/web/js/requirejs-loader.js`

Step 5 - Install r.js
---
Run `npm install -g requirejs`. For further instructions see https://github.com/requirejs/r.js/blob/master/README.md.

Step 6 - Build the optimized bundles
---
Make sure to switch to production mode (`bin/magento deploy:mode:set production`), otherwise the bundle loader will not be included.

To optimize the RequireJS files, you first need to deploy the static content (`bin/magento setup:static-content:deploy`), when completed run the optimizer script: 

```bin/require-js-optimizer.sh```


Step 7 - Optional, only if you deploy to Magento Commerce Cloud / platform.sh
---
In order to run the optimizer during the deploy (in the build-phase) update your `.magento.app.yaml` (or `.platform.app.yaml`) file:

```
hooks:
    # We run build hooks before your application has been packaged.
    build: |
        set -e
        php ./vendor/bin/ece-tools build:generate
        bin/require-js-optimizer.sh
        php ./vendor/bin/ece-tools build:transfer
    # We run deploy hook after your application has been deployed and started.
    deploy: |
        php ./vendor/bin/ece-tools deploy
    # We run post deploy hook to clean and warm the cache. Available with ECE-Tools 2002.0.10.
    post_deploy: |
        php ./vendor/bin/ece-tools post-deploy
``` 

Also you need to disable the GZIP-ing of the static content during the ece-tools build:generate command, do disable the GZIP update the `.magento.env.yaml` file:

```
          stage:
            build:
              SCD_COMPRESSION_LEVEL: 0
```